<?php

/*
------------使用示例二：对用户回复的消息解密---------------
用户回复消息或者点击事件响应时，企业会收到回调消息，此消息是经过公众平台加密之后的密文以post形式发送给企业，密文格式请参考官方文档
假设企业收到公众平台的回调消息如下：
POST /cgi-bin/wxpush?msg_signature=1f3153b7abd0ba99b4ab2e57fac2376110d03a3c&timestamp=1409659813&nonce=1372623149 HTTP/1.1

{
	"ToUserName": "wx5823bf96d3bd56c7",
	"Encrypt": "jYKl5gAKBiSvZ694aryRMNxKJhUJFtNCSDS9TgfV7rDtEe0x6FjiuCWenK3dCDOah+qOJ8yS6RERDoFhe4dYsHpyImaoZwiGjTp1RGXr7GEW5Tn21BdmYId4Pzvoi6ieOKWbrzag5v2TzcF9syQtry2Ujg5hLEgmMP1Y3GPKHLJ8Rg1kpASRriNKeoHWnokLHlpVt3Ai45y2Bfqn+GxT7qz+yODK3f9Ygxhkpkvp6EaIDIzvk77r26t6Q/sTGfzBYPsNYI8t811B9UFyr38gwslPQUHYuOUXalAUnqpiZW0=";
	"AgentID": 218
}

企业收到post请求之后应该
1.解析出url上的参数，包括消息体签名(msg_signature)，时间戳(timestamp)以及随机数字串(nonce)
2.验证消息体签名的正确性。
3.将post请求的数据进行xml解析，并将<Encrypt>标签的内容进行解密，解密出来的明文即是用户回复消息的明文，明文格式请参考官方文档
第2，3步可以用公众平台提供的库函数DecryptMsg来实现。
*/

$sReqMsgSig = "1f3153b7abd0ba99b4ab2e57fac2376110d03a3c";
$sReqTimeStamp = "1409659813";
$sReqNonce = "1372623149";

// post请求的密文数据
$sReqData = '{ "ToUserName": "wx5823bf96d3bd56c7", "Encrypt": "pvXgeha3nZ7UQ2DG3WD3IVsNJR38QVl1+h+wmLQHIAwEG3Jey2aTZtvAWzhEZ9JEKGwIUMBWX3JlUhfo7O7WbvI10vHIsdSNr1nr3LuOwAfnwLJAMQNDxi5tHUl08Op59jxOL73r8+g60KOxMTaFwXzlr9Yxgq5ey5nzNb/2vDjjvDZNXkJVmL/XWnv6PXrVbTpNxphqWRv2BpPYqvwRMkHgdFliatljMVlkkRHS40FOHmtBhj/4RXtIexfDx8opBXwi0L9RTlYNQzGx+FA+74xVHG7Le/pmNDwc2Ri5mbw=", "AgentID": 218 }';
$sMsg = "";  // 解析之后的明文
$errCode = $wxcpt->DecryptMsg($sReqMsgSig, $sReqTimeStamp, $sReqNonce, $sReqData, $sMsg);
if ($errCode == 0) {
    // 解密成功，sMsg即为xml格式的明文
    print("done DecryptMsg, sMsg : \n");
    var_dump($sMsg);
    // TODO: 对明文的处理
} else {
    print("ERR: ".$errCode."\n\n");
    //exit(-1);
}

print("===============================\n");
