<?php

namespace tests\wework\api;

/*
 * Copyright (C) 2017 All rights reserved.
 *
 * @File ApprovalTest.php
 * @Brief
 * @Author abelzhu, abelzhu@tencent.com
 * @Version 1.0
 * @Date 2017-12-27
 *
 */

use tests\wework\TestCase;
use wework\CorpAPI;
use Exception;

final class ApprovalTest extends TestCase
{

    protected function setUp(): void
    {
        parent::setUp();

        $this->api = new CorpAPI($this->config['CORP_ID'], $this->config['APPROVAL_APP_SECRET']);
    }

    public function testApprovalDataGet()
    {
        $ApprovalDataList = $this->api->ApprovalDataGet(1513649733, 1513770113);
        var_dump($ApprovalDataList);
    }
}


