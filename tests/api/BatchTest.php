<?php

namespace tests\wework\api;

/*
 * Copyright (C) 2017 All rights reserved.
 *
 * @File BatchTest.php
 * @Brief
 * @Author abelzhu, abelzhu@tencent.com
 * @Version 1.0
 * @Date 2017-12-26
 *
 */

use tests\wework\TestCase;
use Exception;
use wework\struct\batch\BatchJobArgs;

final class BatchTest extends TestCase
{

    public function testBatchSyncUser()
    {
        $batchJobArgs = new BatchJobArgs();
        {
            $batchJobArgs->media_id = "1yyrBujtAp1U04xwuoevYqSEK0osexgr900H9iP4xBrdj0QVWgl2Jc-0u-F3S7SJVXKSslr10C0YgAlfdKKganA";
            $batchJobArgs->callback->url = "www.qq.com";
            $batchJobArgs->callback->token = "token";
        }
        $jobId = $this->api->BatchSyncUser($batchJobArgs);
        echo $jobId."\n";

        $jobResult = $this->api->BatchJobGetResult($jobId);
        var_dump($jobResult);

    }
}
