<?php

namespace tests\wework\api;

/*
 * Copyright (C) 2017 All rights reserved.
 *
 * @File MenuTest.php
 * @Brief
 * @Author abelzhu, abelzhu@tencent.com
 * @Version 1.0
 * @Date 2017-12-26
 *
 */

use tests\wework\TestCase;

use wework\struct\Menu;
use wework\struct\menu\SubMenu;
use wework\struct\menu\ViewMenu;
use wework\struct\menu\ScanCodePushMenu;
use wework\struct\menu\PicWeixinMenu;
use Exception;

final class MenuTest extends TestCase
{

    public function testMenuCreate()
    {
        $agentId = $this->config['AGENT_ID'];

        $subMenu = new SubMenu(
            "subMenu_1",
            array(
                new ViewMenu("viewMenu_1", "www.qq.com"),
                new ViewMenu("viewMenu_2", "www.baidu.com")
            )
        );
        $scanCodePushMenu = new ScanCodePushMenu(
            "ScanCodePushMenu",
            null,
            array(
                new ViewMenu("viewMenu_3", "www.qq.com"),
                new PicWeixinMenu("PicWeixinMenu", "keykeykey", null),
            )
        );

        $menu = new Menu(
            array(
                $subMenu,
                $scanCodePushMenu
            )
        );
        $this->api->MenuCreate($agentId, $menu);

        //
        $menu = $this->api->MenuGet($agentId);
        var_dump($menu);

        //
        $this->api->MenuDelete($agentId);

    }
}
