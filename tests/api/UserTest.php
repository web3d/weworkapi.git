<?php

namespace tests\wework\api;

/*
 * Copyright (C) 2017 All rights reserved.
 *
 * @File UserTest.php
 * @Brief
 * @Author abelzhu, abelzhu@tencent.com
 * @Version 1.0
 * @Date 2017-12-26
 *
 */

use tests\wework\TestCase;
use wework\CorpAPI;

use wework\struct\User;
use wework\struct\user\ExtattrList;
use wework\struct\user\ExtattrItem;
use Exception;

final class UserTest extends TestCase
{

    protected $departmentId;

    protected function setUp(): void
    {
        parent::setUp();

        $this->api = new CorpAPI($this->config['CORP_ID'], $this->config['CONTACT_SYNC_SECRET']);

        $this->departmentId = 9;
    }

    public function testUserCreatee()
    {
        $user = new User();
        {
            $user->userid = "userid";
            $user->name = "name";
            $user->mobile = "131488888888";
            $user->email = "sbzhu@ipp.cas.cn";
            $user->department = array(1);

            $ExtattrList = new ExtattrList();
            $ExtattrList->attrs = array(new ExtattrItem("s_a_2", "aaa"), new ExtattrItem("s_a_3", "bbb"));
            $user->extattr = $ExtattrList;
        }
        $this->api->UserCreate($user);

        //
        $user = $this->api->UserGet("userid");
        var_dump($user);

        //
        $user->mobile = "1219887219873";
        $this->api->UserUpdate($user);

        //
        $userList = $this->api->userSimpleList(1, 0);
        var_dump($userList);

        //
        $userList = $this->api->UserList(1, 0);
        var_dump($userList);

        //
        $openId = null;
        $this->api->UserId2OpenId("ZhuShengBen", $openId);
        echo "openid: $openId\n";

        //
        $userId = null;
        $this->api->openId2UserId($openId, $userId);
        echo "userid: $userId\n";

        //
        $this->api->UserAuthSuccess("userid");

        //
        $this->api->UserBatchDelete(array("userid", "aaa"));

        //
        $this->api->UserDelete("userid");
    }
}
