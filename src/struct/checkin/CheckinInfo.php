<?php

namespace wework\struct\checkin;

use wework\Utils;

class CheckinInfo
{
    /** @var string */
    public $userid = null;
    /** @var CheckinGroup */
    public $group = null;

    static public function ParseFromArray($arr)
    {
        $info = new CheckinInfo();

        $info->userid = Utils::arrayGet($arr, "userid");
        $info->group = CheckinGroup::ParseFromArray($arr["group"]);

        return $info;
    }
}
