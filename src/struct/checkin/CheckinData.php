<?php

namespace wework\struct\checkin;

use wework\Utils;

class CheckinData
{
    /** @var string */
    public $userid = null;
    /** @var string */
    public $groupname = null;
    /** @var string */
    public $checkin_type = null;
    /** @var string */
    public $exception_type = null;
    /** @var int */
    public $checkin_time = null;
    /** @var string */
    public $location_title = null;
    /** @var string */
    public $location_detail = null;
    /** @var string */
    public $wifiname = null;
    /** @var string */
    public $notes = null;
    /** @var string */
    public $wifimac = null;
    /** @var string|array */
    public $mediaids = null;

    static public function ParseFromArray($arr)
    {
        $info = new CheckinData();

        $info->userid = Utils::arrayGet($arr, "userid");
        $info->groupname = Utils::arrayGet($arr, "groupname");
        $info->checkin_type = Utils::arrayGet($arr, "checkin_type");
        $info->exception_type = Utils::arrayGet($arr, "exception_type");
        $info->checkin_time = Utils::arrayGet($arr, "checkin_time");
        $info->location_title = Utils::arrayGet($arr, "location_title");
        $info->location_detail = Utils::arrayGet($arr, "location_detail");
        $info->wifiname = Utils::arrayGet($arr, "wifiname");
        $info->notes = Utils::arrayGet($arr, "notes");
        $info->wifimac = Utils::arrayGet($arr, "wifimac");
        $info->mediaids = Utils::arrayGet($arr, "mediaids");

        return $info;
    }
}
