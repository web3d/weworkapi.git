<?php

namespace wework\struct\checkin;

use wework\Utils;

class CheckinDate
{
    /** @var int[]|array */
    public $workdays = null;
    /**
     * @var CheckinTime[]|array
     */
    public $checkintime = null;
    /** @var int */
    public $flex_time = null;
    /** @var bool */
    public $noneed_offwork = null;
    /** @var int */
    public $limit_aheadtime = null;

    public static function ParseFromArray($arr)
    {
        $info = new CheckinDate();

        $info->workdays = Utils::arrayGet($arr, "workdays");
        foreach ($arr["checkintime"] as $item) {
            $info->checkintime[] = CheckinTime::ParseFromArray($item);
        }
        $info->flex_time = Utils::arrayGet($arr, "flex_time");
        $info->noneed_offwork = Utils::arrayGet($arr, "noneed_offwork");
        $info->limit_aheadtime = Utils::arrayGet($arr, "limit_aheadtime");

        return $info;
    }
}
