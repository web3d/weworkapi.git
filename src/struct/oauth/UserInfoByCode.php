<?php

namespace wework\struct\oauth;

use wework\Utils;

class UserInfoByCode
{
    /** @var string */
    public $UserId = null;
    /** @var string */
    public $DeviceId = null;
    /** @var string */
    public $user_ticket = null;
    /** @var int */
    public $expires_in = null;
    /** @var string */
    public $OpenId = null;

    static public function Array2UserInfoByCode($arr)
    {
        $info = new UserInfoByCode();

        $info->UserId = Utils::arrayGet($arr, "UserId");
        $info->DeviceId = Utils::arrayGet($arr, "DeviceId");
        $info->user_ticket = Utils::arrayGet($arr, "user_ticket");
        $info->expires_in = Utils::arrayGet($arr, "expires_in");
        $info->OpenId = Utils::arrayGet($arr, "OpenId");

        return $info;
    }
}
