<?php

namespace wework\struct\oauth;

use wework\Utils;

class UserDetailByUserTicket
{
    /** @var string */
    public $userid = null;
    /** @var string */
    public $name = null;
    /** @var int[]|array */
    public $department = null;
    /** @var string */
    public $position = null;
    /** @var string, 成员手机号，仅在用户同意snsapi_privateinfo授权时返回 */
    public $mobile = null;
    /** @var int, 性别。0表示未定义，1表示男性，2表示女性 */
    public $gender = null;
    /** @var string */
    public $email = null;
    /** @var string, 头像url。注：如果要获取小图将url最后的”/0”改成”/100”即可 */
    public $avatar = null;

    static public function Array2UserDetailByUserTicket($arr)
    {
        $info = null;

        $info->userid = Utils::arrayGet($arr, "userid");
        $info->name = Utils::arrayGet($arr, "name");
        $info->department = Utils::arrayGet($arr, "department");
        $info->position = Utils::arrayGet($arr, "position");
        $info->mobile = Utils::arrayGet($arr, "mobile");
        $info->gender = Utils::arrayGet($arr, "gender");
        $info->email = Utils::arrayGet($arr, "email");
        $info->avatar = Utils::arrayGet($arr, "avatar");

        return $info;
    }
}
