<?php

namespace wework\struct\serviceProvider;

use wework\Utils;

class GetRegisterCodeReq
{
    /** @var string */
    public $template_id = null;
    /** @var string */
    public $corp_name = null;
    /** @var string */
    public $admin_name = null;
    /** @var string */
    public $admin_mobile = null;

    public function FormatArgs()
    {
        Utils::checkNotEmptyStr($this->template_id, "template_id");

        $args = array();

        Utils::setIfNotNull($this->template_id, "template_id", $args);
        Utils::setIfNotNull($this->corp_name, "corp_name", $args);
        Utils::setIfNotNull($this->admin_name, "admin_name", $args);
        Utils::setIfNotNull($this->admin_mobile, "admin_mobile", $args);

        return $args;
    }
}
