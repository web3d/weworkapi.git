<?php

namespace wework\struct\serviceProvider;

use wework\Utils;

class LoginAgentInfo
{
    /** @var int */
    public $agentid = null;
    /** @var int */
    public $auth_type = null;

    static public function ParseFromArray($arr)
    {
        $info = new LoginAgentInfo();

        $info->agentid = Utils::arrayGet($arr, "agentid");
        $info->auth_type = Utils::arrayGet($arr, "auth_type");

        return $info;
    }
}
