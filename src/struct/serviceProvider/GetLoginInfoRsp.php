<?php

namespace wework\struct\serviceProvider;

use wework\Utils;

class GetLoginInfoRsp
{
    /** @var int */
    public $usertype = null;
    /** @var LoginUserInfo */
    public $user_info = null;
    /** @var LoginCorpInfo */
    public $corp_info = null;
    /** @var LoginAgentInfo[]|array */
    public $agent = null;
    /** @var LoginAuthInfo */
    public $auth_info = null;

    static public function ParseFromArray($arr)
    {
        $info = new GetLoginInfoRsp();

        $info->usertype = Utils::arrayGet($arr, "usertype");

        if (array_key_exists("user_info", $arr)) {
            $info->user_info = LoginUserInfo::ParseFromArray($arr["user_info"]);
        }
        if (array_key_exists("corp_info", $arr)) {
            $info->corp_info = LoginCorpInfo::ParseFromArray($arr["corp_info"]);
        }
        foreach ($arr["agent"] as $item) {
            $info->agent[] = LoginAgentInfo::ParseFromArray($item);
        }
        if (array_key_exists("auth_info", $arr)) {
            $info->auth_info = LoginAuthInfo::ParseFromArray($arr["auth_info"]);
        }

        return $info;
    }
}
