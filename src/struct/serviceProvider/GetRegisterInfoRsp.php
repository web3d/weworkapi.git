<?php

namespace wework\struct\serviceProvider;

use wework\Utils;

class GetRegisterInfoRsp
{
    /** @var string */
    public $corpid = null;
    /** @var ContactSync */
    public $contact_sync = null;
    /** @var RegisterAuthUserInfo */
    public $auth_user_info = null;

    static public function ParseFromArray($arr)
    {
        $info = new GetRegisterInfoRsp();

        $info->corpid = Utils::arrayGet($arr, "corpid");

        if (array_key_exists("contact_sync", $arr)) {
            $info->contact_sync = ContactSync::ParseFromArray($arr["contact_sync"]);
        }
        if (array_key_exists("auth_user_info", $arr)) {
            $info->auth_user_info = RegisterAuthUserInfo::ParseFromArray($arr["auth_user_info"]);
        }

        return $info;
    }
}
