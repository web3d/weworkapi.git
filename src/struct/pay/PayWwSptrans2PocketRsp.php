<?php

namespace wework\struct\pay;

use wework\Utils;

class PayWwSptrans2PocketRsp
{
    /** @var string */
    public $return_code = null;
    /** @var string */
    public $return_msg = null;
    /** @var string */
    public $appid = null;
    /** @var string */
    public $mch_id = null;
    /** @var string */
    public $device_info = null;
    /** @var string */
    public $nonce_str = null;
    /** @var string */
    public $result_code = null;
    /** @var string */
    public $partner_trade_no = null;
    /** @var string */
    public $payment_no = null;
    /** @var string */
    public $payment_time = null;
}
