<?php

namespace wework\struct\pay;

use wework\Utils;

class QueryWorkWxRedpackRsp
{
    /** @var string */
    public $return_code = null;
    /** @var string */
    public $return_msg = null;
    /** @var string */
    public $sign = null;
    /** @var string */
    public $result_code = null;
    /** @var string */
    public $mch_billno = null;
    /** @var string */
    public $mch_id = null;
    /** @var string */
    public $detail_id = null;
    /** @var string */
    public $status = null;
    /** @var string */
    public $send_type = null;
    /** @var int */
    public $total_amount = null;
    /** @var string */
    public $reason = null;
    /** @var string */
    public $send_time = null;
    /** @var string */
    public $wishing = null;
    /** @var string */
    public $remark = null;
    /** @var string */
    public $act_name = null;
    /** @var string */
    public $openid = null;
    /** @var int */
    public $amount = null;
    /** @var string */
    public $rcv_time = null;
    /** @var string */
    public $sender_name = null;
    /** @var string */
    public $sender_header_media_id = null;
}
