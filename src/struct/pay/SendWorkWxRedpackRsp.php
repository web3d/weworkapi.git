<?php

namespace wework\struct\pay;

use wework\Utils;

class SendWorkWxRedpackRsp
{
    /** @var string */
    public $return_code = null;
    /** @var string */
    public $return_msg = null;
    /** @var string */
    public $sign = null;
    /** @var string */
    public $result_code = null;
    /** @var string */
    public $mch_billno = null;
    /** @var string */
    public $mch_id = null;
    /** @var string */
    public $wxappid = null;
    /** @var string */
    public $re_openid = null;
    /** @var int */
    public $total_amount = null;
    /** @var string */
    public $send_listid = null;
    /** @var string */
    public $sender_name = null;
    /** @var string */
    public $sender_header_media_id = null;
}

