<?php

namespace wework\struct\message;

use wework\Utils;

class VideoMessageContent implements MessageContent
{
    public $msgtype = "video";
    /** @var string */
    public $media_id = null;
    /** @var string */
    public $title = null;
    /** @var string */
    public $description = null;

    public function __construct($media_id = null, $title = null, $description = null)
    {
        $this->media_id = $media_id;
        $this->title = $title;
        $this->description = $description;
    }

    public function CheckMessageSendArgs()
    {
        Utils::checkNotEmptyStr($this->media_id, "media_id");
    }

    public function MessageContent2Array(&$arr)
    {
        Utils::setIfNotNull($this->msgtype, "msgtype", $arr);

        $contentArr = array();
        {
            Utils::setIfNotNull($this->media_id, "media_id", $contentArr);
            Utils::setIfNotNull($this->title, "title", $contentArr);
            Utils::setIfNotNull($this->description, "description", $contentArr);
        }
        Utils::setIfNotNull($contentArr, $this->msgtype, $arr);
    }
}
