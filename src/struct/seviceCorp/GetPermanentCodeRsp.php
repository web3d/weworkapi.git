<?php

namespace wework\struct\serviceCorp;

use wework\Utils;

class GetPermanentCodeRsp
{
    /** @var string */
    public $access_token = null;
    /** @var int */
    public $expires_in = null;
    /** @var string */
    public $permanent_code = null;
    /** @var AuthCorpInfo */
    public $auth_corp_info = null;
    /** @var AuthInfo */
    public $auth_info = null;
    /** @var AuthUserInfo */
    public $auth_user_info = null;

    static public function ParseFromArray($arr)
    {
        $info = new GetPermanentCodeRsp();

        $info->access_token = Utils::arrayGet($arr, "access_token");
        $info->expires_in = Utils::arrayGet($arr, "expires_in");
        $info->permanent_code = Utils::arrayGet($arr, "permanent_code");

        if (array_key_exists("auth_corp_info", $arr)) {
            $info->auth_corp_info = AuthCorpInfo::ParseFromArray($arr["auth_corp_info"]);
        }
        if (array_key_exists("auth_info", $arr)) {
            $info->auth_info = AuthInfo::ParseFromArray($arr["auth_info"]);
        }
        if (array_key_exists("auth_user_info", $arr)) {
            $info->auth_user_info = AuthUserInfo::ParseFromArray($arr["auth_user_info"]);
        }

        return $info;
    }
}
