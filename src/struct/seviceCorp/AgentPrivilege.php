<?php

namespace wework\struct\serviceCorp;

use wework\Utils;

class AgentPrivilege
{
    /** @var int */
    public $level = null;
    /** @var int[]|array */
    public $allow_party = null;
    /** @var string|array */
    public $allow_user = null;
    /** @var int[]|array */
    public $allow_tag = null;
    /** @var int[]|array */
    public $extra_party = null;
    /** @var string[]|array */
    public $extra_user = null;
    /** @var int[]|array */
    public $extra_tag = null;

    static public function ParseFromArray($arr)
    {
        $info = new AgentPrivilege();

        $info->level = Utils::arrayGet($arr, "level");
        $info->allow_party = Utils::arrayGet($arr, "allow_party");
        $info->allow_user = Utils::arrayGet($arr, "allow_user");
        $info->allow_tag = Utils::arrayGet($arr, "allow_tag");
        $info->extra_party = Utils::arrayGet($arr, "extra_party");
        $info->extra_user = Utils::arrayGet($arr, "extra_user");
        $info->extra_tag = Utils::arrayGet($arr, "extra_tag");

        return $info;
    }
}
