<?php

namespace wework\struct\serviceCorp;

use wework\Utils;

class GetAdminListRsp
{
    /** @var AppAdmin[]|array */
    public $admin = null;

    static public function ParseFromArray($arr)
    {
        $info = new GetAdminListRsp();

        foreach ($arr["admin"] as $item) {
            $info->admin[] = AppAdmin::ParseFromArray($item);
        }

        return $info;
    }
}
