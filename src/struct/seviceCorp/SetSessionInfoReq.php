<?php

namespace wework\struct\serviceCorp;

use wework\Utils;

class SetSessionInfoReq
{
    /** @var string */
    public $pre_auth_code = null;
    /** @var SessionInfo */
    public $session_info = null;

    public function FormatArgs()
    {
        Utils::checkNotEmptyStr($this->pre_auth_code, "pre_auth_code");

        $args = array();

        $args["pre_auth_code"] = $this->pre_auth_code;
        $args["session_info"] = $this->session_info->FormatArgs();

        return $args;
    }
}
