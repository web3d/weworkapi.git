<?php

namespace wework\struct\approval;

use wework\Utils;

class ExpenseEvent
{
    public $expense_type = null; // int
    public $reason = null; // string
    /**
     * @var ExpenseItem[]|array|null
     */
    public $item = null;

    static public function ParseFromArray($arr)
    {
        $info = new ExpenseEvent();

        $info->expense_type = Utils::arrayGet($arr, "expense_type");
        $info->reason = Utils::arrayGet($arr, "reason");
        foreach ($arr["item"] as $item) {
            $info->item[] = ExpenseItem::ParseFromArray($item);
        }

        return $info;
    }
}
