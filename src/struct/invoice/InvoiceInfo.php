<?php

namespace wework\struct\invoice;

use wework\Utils;

class InvoiceInfo
{
    /** @var string */
    public $card_id = null;
    /** @var string */
    public $begin_time = null;
    /** @var string */
    public $end_time = null;
    /** @var string */
    public $openid = null;
    /** @var string */
    public $type = null;
    /** @var string */
    public $payee = null;
    /** @var string */
    public $detail = null;
    /**
     * @var InvoiceUserInfo
     */
    public $user_info = null;
}
