<?php

namespace wework\struct\invoice;

use wework\Utils;

class InvoiceUserInfo
{
    /** @var string */
    public $fee = null;
    /** @var string */
    public $title = null;
    /** @var string */
    public $billing_time = null;
    /** @var string */
    public $billing_no = null;
    /** @var string */
    public $billing_code = null;
    /**
     * @var BillingInfo[]|array
     */
    public $info = null;
    /** @var string */
    public $fee_without_tax = null;
    /** @var string */
    public $tax = null;
    /** @var string */
    public $detail = null;
    /** @var string */
    public $pdf_url = null;
    /** @var string */
    public $reimburse_status = null;
    /** @var string */
    public $order_id = null;
    /** @var string */
    public $check_code = null;
    /** @var string */
    public $buyer_number = null;
}
