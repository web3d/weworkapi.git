<?php

namespace wework\struct\menu;

use wework\Utils;

class ClickMenu implements MenuItem
{
    public $type = "click";
    /** @var string */
    public $name = null;
    /** @var string */
    public $key = null;

    public function __construct($name = null, $key = null, $xxmenuArray = null)
    {
        $this->name = $name;
        $this->key = $key;
    }

    public static function Array2Menu($arr)
    {
        $menu = new ClickMenu();

        $menu->type = Utils::arrayGet($arr, "type");
        $menu->name = Utils::arrayGet($arr, "name");
        $menu->key = Utils::arrayGet($arr, "key");

        return $menu;
    }
}
